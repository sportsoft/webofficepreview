﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebOfficePreview.Models.ApiViewModels
{
    public class GetViewModel
    {
        public int FileId { get; set; }

        public int Status { get; set; }

        public List<string> Images { get; set; }
    }
}
