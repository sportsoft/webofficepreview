﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebOfficePreview.Models;
using Component_OfficePreview.Data;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;
using Component_OfficePreview.Entities;
using Microsoft.AspNetCore.Authorization;
using Component_OfficePreview;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebOfficePreview.Controllers
{
    public abstract class _Controller : Controller
    {
        public NotifyDictionary<string, string> Notify { get; set; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            ViewBag.Notify = new NotifyDictionary<string, string>();

            if(TempData.ContainsKey("danger"))
                ViewBag.Notify.Add("danger", TempData["danger"].ToString());

            if (TempData.ContainsKey("success"))
                ViewBag.Notify.Add("success", TempData["success"].ToString());

            base.OnActionExecuting(context);
        } 
    }
}
