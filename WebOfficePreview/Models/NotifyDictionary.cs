﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WebOfficePreview.Models
{
    // Аналог Dictionary для хранения нотификаций клиенту, где ключ - тип нотификации (danger, warning, success), значение - коллекция значений.
    // Если ключ не существует, то добавляется новая пара (ключ - коллекция значений) в справочник
    // Если ключ существует, то добавляется новое значение в коллекцию значений
    public class NotifyDictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, IList<TValue>>>
    {
        private IDictionary<TKey, IList<TValue>> container = new Dictionary<TKey, IList<TValue>>();

        public IEnumerable<TValue> this[TKey key]
        {
            get
            {
                return container.ContainsKey(key) ?
                    container[key] :
                    null;
            }
        }

        public void Add(TKey key, TValue value)
        {
            if (!container.ContainsKey(key))
                container.Add(key, new List<TValue>());

            container[key].Add(value);
        }

        public IEnumerator<KeyValuePair<TKey, IList<TValue>>> GetEnumerator()
        {
            foreach (KeyValuePair<TKey, IList<TValue>> pair in container)
            {
                yield return new KeyValuePair<TKey, IList<TValue>>(pair.Key, pair.Value);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return container.GetEnumerator();
        }
    }
}
