﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebOfficePreview.Models;
using Component_OfficePreview.Data;
using Microsoft.AspNetCore.Http;
using Component_OfficePreview.Entities;
using WebOfficePreview.Models.ApiViewModels;
using WebOfficePreview.Services;
using Component_OfficePreview;

namespace WebOfficePreview.Controllers
{
    public class ApiController : Controller
    {
        public readonly ApplicationDbContext _context;
        public readonly FileUploader _fileUploader;

        public ApiController(ApplicationDbContext context, FileUploader fileUploader)
        {
            _context = context;
            _fileUploader = fileUploader;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult Get(string key, int FileId)
        {
            UserDb user = _context.Users.FirstOrDefault(x => x.ApiHash == key);

            if (user is null)
            {
                return GetViewModel(FileId, (int)FileUploaderStatus.ErrorRequireAuthentication);
            }

            FileDb file = _context.Files.FirstOrDefault(x => x.Id == FileId && x.UserId == user.UserId);

            if(file is null)
            {
                return GetViewModel(FileId, (int)FileUploaderStatus.ErrorNotFound);
            }

            return GetViewModel(FileId, (int)file.Status);
        }

        public async Task<JsonResult> Upload(string key, IFormFile file)
        {
            UserDb user = _context.Users.FirstOrDefault(x => x.ApiHash == key);

            if(user is null)
            {
                return Json(new UploadViewModel
                {
                    Status = (int)FileUploaderStatus.ErrorRequireAuthentication
                });
            }

            FileUplaoderResult UploadResult = await _fileUploader.Upload(file, user.UserId);

            return Json(new UploadViewModel
            {
                FileId = UploadResult.FileId,
                Status = (int)UploadResult.Status
            });
        }

        #region -- Helpers

        private JsonResult GetViewModel(int FileId, int Status)
        {
            return Json(new GetViewModel
            {
                FileId = FileId,
                Status = Status
            });
        }

        #endregion

    }
}
