﻿using Component_OfficePreview.Entities;
using Microsoft.EntityFrameworkCore;

namespace Component_OfficePreview.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<FileDb> Files { get; set; }

        public DbSet<UserDb> Users { get; set; }

        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("User ID=postgres;Password=1;Host=localhost;Port=5432;Database=OfficePreviewDb;Pooling=true;", opts => opts.CommandTimeout(300));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
