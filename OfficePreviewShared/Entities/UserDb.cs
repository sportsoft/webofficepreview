﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component_OfficePreview.Entities
{
    public class UserDb
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int UserId { get; set; }

        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public DateTime DateReg { get; set; }

        public DateTime DateLastVisit { get; set; }

        public string ApiHash { get; set; }
    }
}
