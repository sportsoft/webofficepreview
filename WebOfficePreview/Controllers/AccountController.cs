﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebOfficePreview.Models;
using WebOfficePreview.Models.AccountViewModels;
using Component_OfficePreview.Data;
using Component_OfficePreview.Entities;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace WebOfficePreview.Controllers
{
    public class AccountController : _Controller
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public AccountController(ILogger<AccountController> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            if (!ModelState.IsValid)
            {
                ViewBag.Notify.Add("danger", "Ошибка! Проверьте правильность введенных данных.");

                return View(model);
            }

            UserDb ExistedUser = _context.Users.FirstOrDefault(u => u.Email == model.Email && u.PasswordHash == Extensions.GetMD5(model.Password));

            if (ExistedUser == null)
            {
                ViewBag.Notify.Add("danger", "Ошибка! Пользователь с таким именем и паролем не найден в базе данных.");

                return View(model);
            }

            await Authenticate(ExistedUser);

            return RedirectToLocal(returnUrl);
        }

        [HttpGet]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            if ( ! ModelState.IsValid)
            {
                ViewBag.Notify.Add("danger", "Ошибка! Проверьте правильность введенных данных.");

                return View(model);
            }

            UserDb ExistedUser = _context.Users.FirstOrDefault(u => u.Email == model.Email);

            if (ExistedUser != null)
            {
                ViewBag.Notify.Add("danger", "Ошибка! Пользователь с таким e-mail уже зарегистрирован.");

                return View(model);
            }

            var user = new UserDb
            {
                Email = model.Email,
                PasswordHash = Extensions.GetMD5(model.Password),
                DateReg = DateTime.Now,
                DateLastVisit = DateTime.Now
            };

            Random rand = new Random();

            do
            {
                string hashString = rand.Next(10000000, 99999999).ToString() + user.Email;

                user.ApiHash = Extensions.GetMD5(hashString);
            }
            while (_context.Users.Any(x => x.ApiHash == user.ApiHash));

            _context.Users.Add(user);
            _context.SaveChanges();

            await Authenticate(user);

            return RedirectToLocal(returnUrl);
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        #region Helpers

        private async Task Authenticate(UserDb user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim("UserId", user.UserId.ToString()),
                new Claim("ApiHash", user.ApiHash),
            };
            
            ClaimsIdentity ClaimId = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(ClaimId));
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
