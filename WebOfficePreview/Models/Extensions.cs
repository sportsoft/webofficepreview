﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WebOfficePreview.Models
{
    public static class Extensions
    {
        public static string GetMD5(string inputString)
        {
            byte[] byteString = new UTF8Encoding().GetBytes(inputString);
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(byteString);

            return BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
        }

        public static string GetRandString(int length)
        {
            String result = "";

            String symbols = "abcdefghijklmnopqrstuvwxyz_0123456789";

            Random rnd = new Random();

            for (int i = 0; i < length; i++)
            {
                result += symbols[rnd.Next(0, symbols.Length)];
            }

            return result;
        }
    }
}
