﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component_OfficePreview.Entities
{
    public class FileDb
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public FileUploaderStatus Status { get; set; }

        public string Name { get; set; }

        public string Extension { get; set; }

        public string Path { get; set; }

        public long Filesize { get; set; }

        public int Pages { get; set; }

        public DateTime DateUpload { get; set; }

        public string Hash { get; set; }
    }
}
