﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component_OfficePreview
{
    public enum FileUploaderStatus
    {
        [Display(Name = "Загружен")]
        Uploaded = 1,

        [Display(Name = "В обарботке")]
        InProgress = 2,

        [Display(Name = "Ок")]
        Ok = 3,

        [Display(Name = "Ошибка обработки")]
        Error = 4,

        ErrorNoUpload = 902,

        ErrorTooLarge = 903,

        ErrorBadFormat = 904,

        ErrorRequireAuthentication = 905,

        ErrorNotFound = 906
    }
}
