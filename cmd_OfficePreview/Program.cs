﻿using Component_OfficePreview;
using Component_OfficePreview.Data;
using Component_OfficePreview.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using xps2img;

namespace cmd_OfficePreview
{
    class Program
    {
        public static string WebRootPath = @"E:\repo\WebOfficePreview\WebOfficePreview\wwwroot\";

        static void ConvertToXps()
        {
            using (ApplicationDbContext _context = new ApplicationDbContext())
            {
                List<FileDb> files = _context.Files.Where(x => x.Status == FileUploaderStatus.Uploaded).ToList();

                foreach (FileDb t_file in files)
                {
                    string destFile = Path.Combine(WebRootPath, "ConvertedFiles", t_file.Hash.ToString() + ".xps");
                    string sourceFile = Path.Combine(WebRootPath, "Files", t_file.Hash + t_file.Extension);

                    t_file.Status = FileUploaderStatus.Error;

                    if (File.Exists(sourceFile))
                    {
                        OfficeToXpsConversionResult convertResult = OfficeToXps.ConvertToXps(sourceFile, ref destFile);

                        Console.WriteLine("File " + sourceFile + ": " + convertResult.Result);

                        if (convertResult.Result == ConversionResult.OK)
                        {
                            t_file.Status = FileUploaderStatus.InProgress;
                        }
                    }

                    _context.Files.Update(t_file);
                }
                _context.SaveChanges();
            }
        }

        static void ConvertToImage()
        {
            using (ApplicationDbContext _context = new ApplicationDbContext())
            {
                List<FileDb> files = _context.Files.Where(x => x.Status == FileUploaderStatus.InProgress).ToList();

                foreach (FileDb t_file in files)
                {
                    string destFile = Path.Combine(WebRootPath, "ConvertedFiles", t_file.Hash.ToString() + ".xps");
                    int page = 0;

                    if (File.Exists(destFile))
                    {
                        using (var xpsConverter = new Xps2Image(destFile))
                        {
                            var images = xpsConverter.ToBitmap(new Parameters
                            {
                                ImageType = ImageType.Png,
                                Dpi = 300
                            });

                            foreach (var t_image in images)
                            {
                                page++;
                                t_image.Save(Path.Combine(WebRootPath, "ConvertedImages", t_file.Hash + "_" + page.ToString() + ".png"));
                            }
                        }
                    }

                    t_file.Status = (page > 0) ? FileUploaderStatus.Ok : FileUploaderStatus.Error;
                    t_file.Pages = page;

                    _context.Files.Update(t_file);
                }
                _context.SaveChanges();
            }
        }

        static void Main(string[] args)
        {
            while(true)
            {
                ConvertToXps();
                ConvertToImage();

                Console.WriteLine(DateTime.Now + " - Ok");
                Thread.Sleep(5000);
            }
        }
    }
}
