﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebOfficePreview.Models;
using Component_OfficePreview.Data;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;
using Component_OfficePreview.Entities;
using Microsoft.AspNetCore.Authorization;
using Component_OfficePreview;
using Microsoft.AspNetCore.Mvc.Filters;
using WebOfficePreview.Services;

namespace WebOfficePreview.Controllers
{
    public class HomeController : _Controller
    {
        public readonly IHostingEnvironment _environment;
        public readonly ApplicationDbContext _context;
        public readonly FileUploader _fileUploader;

        public HomeController(IHostingEnvironment environment, ApplicationDbContext context, FileUploader fileUploader)
        {
            _environment = environment;
            _context = context;
            _fileUploader = fileUploader;
        }

        [Authorize]
        public IActionResult Cabinet(int? DeleteFileId)
        {
            int userId = Convert.ToInt32(User.Claims.FirstOrDefault(x => x.Type == "UserId").Value);

            #region -- Remove file

            if (DeleteFileId != null)
            {
                FileDb file = _context.Files.FirstOrDefault(x => x.UserId == userId && x.Id == DeleteFileId);

                if (file is null)
                {
                    ViewBag.Notify.Add("danger", "Файл не найден, либо уже удален");
                }
                else
                {
                    string destFile = Path.Combine(_environment.WebRootPath + @"\Files\", file.Hash + file.Extension);
                    string xpsFile = Path.Combine(_environment.WebRootPath + @"\ConvertedFiles\", file.Hash + ".xps");

                    if (System.IO.File.Exists(destFile))
                    {
                        System.IO.File.Delete(destFile);
                    }

                    if (System.IO.File.Exists(xpsFile))
                    {
                        System.IO.File.Delete(xpsFile);
                    }

                    for (int i = 0; i < file.Pages; i++)
                    {
                        string pageFile = Path.Combine(_environment.WebRootPath, "ConvertedImages", file.Hash + "_" + i.ToString() + ".jpg");

                        if (System.IO.File.Exists(pageFile))
                        {
                            System.IO.File.Delete(pageFile);
                        }
                    }

                    _context.Files.Remove(file);
                    _context.SaveChanges();

                    ViewBag.Notify.Add("success", "Файл успешно удален");
                }
            }

            #endregion

            List<FileDb> files = _context.Files.Where(x => x.UserId == userId).OrderByDescending(x => x.DateUpload).ToList();

            return View(files);
        }

        [Route("Show/{FileHash}")]
        public IActionResult Show(string FileHash)
        {
            FileDb file = _context.Files.FirstOrDefault(x => x.Hash == FileHash);

            if (file is null)
            {
                TempData["danger"] = "Ошибка! Такой файл не найден.";

                return RedirectToAction("Index");
            }

            return View(file);
        }

        public IActionResult Index()
        {
            if(User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Cabinet");
            }

            return View(_context.Files.ToList());
        }

        public IActionResult Demo()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddFile(IFormFile uploadedFile)
        {
            int userId = Convert.ToInt32(User.Claims.FirstOrDefault(x => x.Type == "UserId").Value);

            FileUplaoderResult UploadResult = await _fileUploader.Upload(uploadedFile, userId);

            if(UploadResult.Status == FileUploaderStatus.ErrorNoUpload)
            {
                TempData["danger"] = "Ошибка! Вы не выбрали файл.";
            }

            if (UploadResult.Status == FileUploaderStatus.ErrorBadFormat)
            {
                TempData["danger"] = "Ошибка! К загрузке разрешены только файлы Word, Excel и PowerPoint.";
            }

            if (UploadResult.Status == FileUploaderStatus.ErrorTooLarge)
            {
                TempData["danger"] = "Ошибка! Размер файла превышает 10 Мб.";
            }

            if( !TempData.ContainsKey("danger"))
            {
                TempData["success"] = "Файл добавлен в очередь на обработку.";
            }

            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
