﻿using Component_OfficePreview;
using Component_OfficePreview.Data;
using Component_OfficePreview.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebOfficePreview.Models;

namespace WebOfficePreview.Services
{
    public class FileUploader
    {
        private readonly ApplicationDbContext _context;
        public readonly IHostingEnvironment _environment;

        public FileUploader(ApplicationDbContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        public async Task<FileUplaoderResult> Upload(IFormFile uploadedFile, int userId)
        {
            List<string> availableExtensions = new List<string> { ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".pptx" };

            if (uploadedFile == null)
            {
                return new FileUplaoderResult { Status = FileUploaderStatus.ErrorNoUpload };
            }

            if (!availableExtensions.Contains(Path.GetExtension(uploadedFile.FileName)))
            {
                return new FileUplaoderResult { Status = FileUploaderStatus.ErrorBadFormat };
            }

            if (uploadedFile.Length > 1 * 1024 * 1024)
            {
                return new FileUplaoderResult { Status = FileUploaderStatus.ErrorTooLarge };
            }

            FileDb file = new FileDb
            {
                UserId = userId,
                Name = Path.GetFileNameWithoutExtension(uploadedFile.FileName),
                Extension = Path.GetExtension(uploadedFile.FileName),
                Status = FileUploaderStatus.Uploaded,
                Filesize = uploadedFile.Length,
                DateUpload = DateTime.Now,
                Hash = Extensions.GetRandString(10)
            };

            // Check hash
            while (_context.Files.Any(x => x.Hash == file.Hash))
            {
                file.Hash = Extensions.GetRandString(10);
            }

            // путь к папке Files
            String destFileName = Path.Combine(_environment.WebRootPath + @"\Files\", file.Hash + file.Extension);

            // сохраняем файл в папку Files в каталоге wwwroot
            using (var fileStream = new FileStream(destFileName, FileMode.Create))
            {
                await uploadedFile.CopyToAsync(fileStream);
            }

            _context.Files.Add(file);
            await _context.SaveChangesAsync();

            return new FileUplaoderResult { FileId = file.Id, Status = FileUploaderStatus.Uploaded };
        }
    }

    public class FileUplaoderResult
    {
        public int FileId { get; set; }

        public FileUploaderStatus Status { get; set; }
    }
}
